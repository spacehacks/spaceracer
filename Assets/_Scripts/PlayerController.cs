﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Timers;
using System.Text;
using uWaveAlgorithm;

public class PlayerController : NetworkBehaviour {

	static int currentId = 0;

	private Rigidbody rb;
	private Animation anim;
	public float speed;

	[SyncVar]
	public int id = 0;

	public GameObject explosion;

	private GameObject bullet;
	private GameObject camera;
	private GameObject light;

	private IList<GameObject> myBalls = new List<GameObject>();

	private Vector3 spawnPoint;	
	private Quaternion spawnRotation;
	//public bool triggerShot;
	private bool canCollectBall = true;


	private int hitCount = 0;

	private Facade gestureRecognizer;
	private Gyroscope gyro;
	private Vector3 acc;
	private bool init;
	private const float TURNING = 12f;

	double originTime;
	bool initBall;


	void Start () 
	{
		originTime = 0.1088037;


		initBall = true;

		Cmd_GetNextId ();

		spawnPoint = transform.position;
		spawnRotation = transform.rotation;

		rb = GetComponent<Rigidbody> ();

		if (isLocalPlayer) {
			camera = (GameObject) Resources.Load ("FollowCamera");
			GameObject theCam = Instantiate (camera) as GameObject;
			theCam.transform.parent = rb.transform;
			theCam.transform.localPosition = new Vector3(0, 7, 18);
			theCam.transform.localRotation = new Quaternion(0, 180, 0, 0);

			light = (GameObject) Resources.Load("FollowLight");
			GameObject theLight = Instantiate (light) as GameObject;
			theLight.transform.parent = rb.transform;
			theLight.transform.localPosition = new Vector3(0, 7, 10);
			theLight.transform.rotation = new Quaternion(5.30f, 4.15f, 0, 9.97f );
		}
		Screen.orientation = ScreenOrientation.LandscapeLeft;

		anim = GetComponent<Animation> ();
		bullet = (GameObject) Resources.Load ("BulletPrefab");

		InitGestures ();

		Cmd_InitializeFire ();
		Cmd_InitializeBall ();
	}

	void InitGestures() {
	//	Time.fixedDeltaTime = 0.002f;
		var templates = new StringBuilder ();
		templates.AppendLine ("Motionless,");
		templates.AppendLine ("Fire,");
		templates.AppendLine ("1|1|2,1|-1|5,2|-2|12,2|-1|16,2|2|16,-1|3|13,-3|2|5,-6|-1|-5,-7|-2|-11,-7|-2|-14,-7|-3|-15,-6|-3|-15,-5|-3|-13,-4|-2|-11,-3|-1|-8,-2|1|-4,-1|1|-1,1|2|2,1|1|2,");
		templates.AppendLine ("-1|-2|-1,-2|-2|1,-2|-2|1,-1|-1|-1,-1|-1|-1,1|-1|-1,2|1|-1,1|1|-1,1|2|-2,-1|2|-2,-1|2|-2,-2|2|-2,-1|1|-2,-1|1|-1,-1|1|2,1|1|4,2|1|8,2|-1|11,2|1|11,1|1|6,1|1|2,-4|1|-11,-4|1|-12,-4|-1|-13,-3|1|-11,-2|1|-5,-2|2|1,-2|1|4,-2|1|5,-1|-1|4,1|-1|2,1|-1|1,1|-1|1,");
		templates.AppendLine ("1|-1|5,1|-2|10,2|-2|10,1|-1|6,-2|2|-3,-3|3|-9,-4|1|-11,-3|-1|-11,-2|-1|-9,-1|1|-4,-1|1|-1,-1|1|2,-1|-1|2,-1|-1|2,1|-1|1,1|-1|1,1|-1|1,");
		templates.Append("1|-1|3,1|-1|3,1|1|3,1|1|4,1|1|5,1|1|5,1|1|6,2|1|8,2|1|9,2|1|7,2|1|2,-1|2|-5,-3|1|-10,-4|1|-12,-4|-1|-12,-4|-1|-10,-3|-1|-4,-3|-1|2,-2|-1|6,-1|-1|6,1|-1|6,2|-1|4,2|1|2,2|1|1,");
		gestureRecognizer = new Facade(1);
		gestureRecognizer.ImportGestureLibrary(templates.ToString());
		gestureRecognizer.GestureRecognized += ActOnGesture;
		
		var results = gestureRecognizer.ExportGestureLibrary ();
		
		if (SystemInfo.supportsGyroscope) {
			gyro = Input.gyro;
			gyro.enabled = true;
			init = true;
		}
	}

	void ActOnGesture(object sender, GestureRecognizedEventArgs args)
	{
		
		if (!args.IsActualGesture) return;
		var id = args.GestureId.ToLowerInvariant();
		switch (id)
		{
		case "fire":
			FireShot();
			break;
		default:
			break;
		}
	}

	[Command]
	void Cmd_GetNextId() {
		id = currentId++;
	}

	private bool shot = false;
	void Update () {
		if ((Input.GetKeyDown ("space")) && !shot && isLocalPlayer) {

			FireShot();

			//print ("SHOT TRIGGERD" + anim.GetBool ("triggerShot"));
			//print ("Found Ship!");
	//		GameObject projectile = (GameObject) Instantiate (bullet);
	//		projectile.transform.position = new Vector3 (transform.position.x + transform.forward.x * -55, transform.position.y + transform.forward.y * -55, transform.position.z + transform.forward.z * -55);

	//		Rigidbody rb = projectile.GetComponent<Rigidbody> ();
	//		rb.velocity = - transform.forward * 2000;
	//		GameObject ship = GameObject.Find ("SpaceShip");

//			SphereBehaviour b = projectile.GetComponent<SphereBehaviour>(); 
//			b.isMyBullet = true;

		} 
		if (Input.GetKeyDown(KeyCode.R)) {
			transform.position = spawnPoint;
			transform.rotation = spawnRotation;

			foreach (var ball in myBalls) {
				if (ball != null) {
					StickBalltoNose(ball);
				}
			}
			//anim.SetBool ("triggerShot", false);
		}

	}

	public void FireShot() {
		if (!isLocalPlayer) {
			return;
		}

		shot = true;
		
		anim.Play();

		canCollectBall = false;
		Timer timer = new Timer(1000);
		timer.AutoReset = false;
		timer.Elapsed += delegate {
			canCollectBall = true;
			timer.Dispose();
		};
		timer.Start();
		
		Cmd_Shoot (new Vector3 (transform.position.x + transform.forward.x * -55, transform.position.y + transform.forward.y * -55, transform.position.z + transform.forward.z * -55));

	}

	[Command]
	void Cmd_InitializeBall() {
		GameObject projectile = (GameObject) Instantiate (bullet);
		projectile.transform.position = rb.position;
		
		Rigidbody ballrb = projectile.GetComponent<Rigidbody> ();
		
		ballrb.velocity = transform.forward * 500;
		
		SphereBehaviour b = projectile.GetComponent<SphereBehaviour>(); 
		b.bulletId = id;
		
		//		projectile.renderer.material.color = 
		
		NetworkServer.Spawn(projectile);
	}
	
	[Command]
	void Cmd_InitializeFire() {
		GameObject torch = Resources.Load ("ShipTorch") as GameObject;
		GameObject instance = Instantiate (torch) as GameObject;
		instance.transform.parent = rb.transform;
		instance.transform.rotation = rb.transform.rotation;
		instance.transform.localPosition = new Vector3 (0, 0, 5);
		instance.transform.localScale = new Vector3 (3, 3, 3);
//		NetworkServer.Spawn (instance);
	}

	[Command]
	void Cmd_Shoot(Vector3 position) {
//		GameObject projectile = (GameObject)Instantiate (bullet);
		foreach (GameObject ball in myBalls) {
			Cmd_DeleteBall(ball);
		}

		GameObject projectile = (GameObject) Instantiate (bullet);
		projectile.transform.position = position;

		Rigidbody rb = projectile.GetComponent<Rigidbody> ();

		rb.velocity = - transform.forward * 2000;

		SphereBehaviour b = projectile.GetComponent<SphereBehaviour>(); 
		b.bulletId = id;

		myBalls.Add (projectile);

//		projectile.renderer.material.color = 

 		NetworkServer.Spawn(projectile);
		
	}

	

	void FixedUpdate () {

		if (!isLocalPlayer) {
			return;
		}

		if (init) {
			gestureRecognizer.AccelerometerInput (gyro.userAcceleration.x, gyro.userAcceleration.y, gyro.userAcceleration.z);
		}


		float moveForward = Input.GetAxis ("Vertical");
		float moveLeftRight = Input.GetAxis ("Horizontal");

		if (moveForward > 0 || Input.touches.Length > 0) {
			MoveForward();
		}

		float angle = 0;
		try {
			var temp = Input.acceleration.normalized;
			temp.z = 0;
			temp = temp.normalized;
			angle = Mathf.Sin((temp.x) / (1 * temp.y));
			angle = angle * 180 / (2 * Mathf.PI);
		} catch (UnityException e) {
			//
		}
	

		if (moveLeftRight > 0 || angle < -12f) {
			TurnLeft();
		} else if (moveLeftRight < 0 || angle > 12f) {
			TurnRight();
		}

		UpdateOrientation();

		//Vector3 movement = new Vector3 (0.0f, 0.0f, moveVertical);

		//rb.AddForce (speed * movement);

		//rb.rotation = Quaternion.Euler (0.0f, moveHorizontal * 100, 0.0f);
	}

	float myGravity = 9.81f;
	void OnCollisionStay(Collision col)
	{
		Vector3 n = Vector3.zero;
		foreach (ContactPoint C in col.contacts)
			n += C.normal;
		n.Normalize();
		
		Physics.gravity = -n*myGravity;
		
		
	}

	public void UpdateOrientation() {


		float distForward = Mathf.Infinity;
		RaycastHit hitForward;
		if (Physics.SphereCast(transform.position, 0.25f, -transform.up + transform.forward, out hitForward, 5))
		{
			distForward = hitForward.distance;
		}
		float distDown = Mathf.Infinity;
		RaycastHit hitDown;
		if (Physics.SphereCast(transform.position, 0.25f, -transform.up, out hitDown, 5))
		{
			distDown = hitDown.distance;
		}
		float distBack = Mathf.Infinity;
		RaycastHit hitBack;
		if (Physics.SphereCast(transform.position, 0.25f, -transform.up + -transform.forward, out hitBack, 5))
		{
			distBack = hitBack.distance;
		}
		
		if (distForward < distDown && distForward < distBack)
		{
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(Vector3.Cross(transform.right, hitForward.normal), hitForward.normal), Time.deltaTime * 5.0f);
		}
		else if (distDown < distForward && distDown < distBack)
		{
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(Vector3.Cross(transform.right, hitDown.normal), hitDown.normal), Time.deltaTime * 5.0f);
		}
		else if (distBack < distForward && distBack < distDown)
		{
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(Vector3.Cross(transform.right, hitBack.normal), hitBack.normal), Time.deltaTime * 5.0f);
		}

		Physics.gravity = - transform.up * myGravity;
		//rb.AddForce(transform.up.x * 3000, transform.up.y * 3000, transform.up.z * 3000);
	}

	public void MoveForward() {
		if (!isLocalPlayer) {
			return;
		}
		transform.Translate(Vector3.forward * 0.3f * speed);
	}

	public void TurnLeft() {
		if (!isLocalPlayer) {
			return;
		}
		rb.transform.Rotate(0.0f, 2.0f, 0.0f);
	}

	public void TurnRight() {
		if (!isLocalPlayer) {
			return;
		}
		rb.transform.Rotate(0.0f, -2.0f, 0.0f);
	}

	bool soundPlaying;

	void OnCollisionEnter (Collision col)
	{
		if(col.gameObject.name.Contains("BulletPrefab"))
		{
			if (col.gameObject.GetComponent<SphereBehaviour>().bulletId == id) {
				if (!canCollectBall) {
					return;
				}

				foreach (var ball in myBalls) {
					if (ball != null && ball != col.gameObject) {
						Cmd_DeleteBall(ball);
					}
				}

				StickBalltoNose(col.gameObject);

				

				if (initBall) {
					initBall = false;
					rb.transform.position = spawnPoint;
					rb.transform.rotation = spawnRotation;
				}
			} else {

				soundPlaying = true;
				GameObject fire = Instantiate(explosion, transform.position, transform.rotation) as GameObject;
				fire.transform.parent = rb.transform;
				Timer timer = new Timer(3000);
				timer.AutoReset = false;
				timer.Elapsed += (sender, e) => {
					Destroy (explosion);
					soundPlaying = false;
				};

				rb.transform.position = spawnPoint;
				rb.transform.rotation = spawnRotation;
				
				timer.Start();

				AudioSource[] srcs = GetComponents<AudioSource>();

				

				srcs[hitCount + 2].Play();

				hitCount = (hitCount + 1) % 5;

			}
		}
	}

	void StickBalltoNose(GameObject ball) {
		ball.GetComponent<Rigidbody>().isKinematic = false;
		ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
		ball.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
		ball.transform.parent = rb.transform;
		ball.transform.localPosition = new Vector3(0, 0, -20);
		
		AudioSource[] srcs = GetComponents<AudioSource>();
		
		srcs[1].Play();
		
		if (!myBalls.Contains (ball)) {
			myBalls.Add (ball);
		}

		shot = false;
	}

	[Command]
	void Cmd_DeleteBall(GameObject obj) {
		Destroy (obj);
	}
}
