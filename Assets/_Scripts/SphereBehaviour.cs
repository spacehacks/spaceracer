﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class SphereBehaviour : NetworkBehaviour {

	Rigidbody rb;

	public AudioClip impact;
	AudioSource audio;

	bool colorSet;

	[SyncVar]
	public int bulletId = -1;
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();
		audio.PlayOneShot(impact, 1.0F);
	}
	
	// Update is called once per frame
	void Update () {
		if (!colorSet && bulletId != -1) {
			var intensity = (50 * (bulletId + 2)) % 256;
			var colorChooser = bulletId % 3;
			Color color = new Color ();
			switch (colorChooser) {
			case 0:
				color = new Color(intensity, 0, 0, 140);
				break;
			case 1:
				color = new Color(0, intensity, 0, 140);
				break;
			case 2:
				color = new Color(0, 0, intensity, 140);
				break;
			default:
				break;
			}
			Renderer rend = GetComponent<Renderer> ();
			//		rend.material.shader = Shader.Find ("Standard");
			rend.material.color = color;
			colorSet = true;
		}

		if (!base.isServer) {
			return;
		}

		try {
		rb.transform.Translate (Vector3.forward * -20.3f);

		float distForward = Mathf.Infinity;
		RaycastHit hitForward;
		if (Physics.SphereCast(transform.position, 0.25f, -transform.up + transform.forward, out hitForward, 5))
		{
			distForward = hitForward.distance;
		}
		float distDown = Mathf.Infinity;
		RaycastHit hitDown;
		if (Physics.SphereCast(transform.position, 0.25f, -transform.up, out hitDown, 5))
		{
			distDown = hitDown.distance;
		}
		float distBack = Mathf.Infinity;
		RaycastHit hitBack;
		if (Physics.SphereCast(transform.position, 0.25f, -transform.up + -transform.forward, out hitBack, 5))
		{
			distBack = hitBack.distance;
		}
		
		if (distForward < distDown && distForward < distBack)
		{
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(Vector3.Cross(transform.right, hitForward.normal), hitForward.normal), Time.deltaTime * 5.0f);
		}
		else if (distDown < distForward && distDown < distBack)
		{
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(Vector3.Cross(transform.right, hitDown.normal), hitDown.normal), Time.deltaTime * 5.0f);
		}
		else if (distBack < distForward && distBack < distDown)
		{
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(Vector3.Cross(transform.right, hitBack.normal), hitBack.normal), Time.deltaTime * 5.0f);
		}
		
		rb.AddForce(-transform.up * 10000);
		} catch (Exception e) {
		}
	}
}
