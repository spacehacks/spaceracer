﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;
using UnityEngine.Networking.Match;
using System.Collections.Generic;
using UnityEngine.UI;

public class HostGame : MonoBehaviour
{
	List<MatchDesc> matchList = new List<MatchDesc>();
	bool matchCreated;
	NetworkManager manager;
	NetworkMatch networkMatch;
	Text text;
	
	long currentNetId;
	long currentNodeId;
	
	void Awake()
	{
		text = GetComponent ("Text") as Text;
		//		text.text = "Awoken";
		
		Debug.Log ("Awoken");
		
		manager = GetComponent<NetworkManager> ();
		
		//		PlayerPrefs.SetString ("CloudNetworkingId", "432602");
		manager.StartMatchMaker ();
		//		manager.matchMaker.SetProgramAppID ((UnityEngine.Networking.Types.AppID)432602);
		
		
		networkMatch = gameObject.AddComponent<NetworkMatch>();
		networkMatch.ListMatches(0, 20, "", OnMatchList);
	}
	
	
	public void CreateMatch() {
		//		text.text = "Creating";
		CreateMatchRequest create = new CreateMatchRequest();
		create.name = "NewRoom";
		create.size = 4;
		create.advertise = true;
		create.password = "";
		
		networkMatch.CreateMatch(create, OnMatchCreate);
	}
	
	public void OnMatchCreate(CreateMatchResponse matchResponse)
	{
		if (matchResponse.success)
		{
			//           text.text = "Create match succeeded with id: " + matchResponse.networkId;
			matchCreated = true;
			currentNetId = (long)matchResponse.networkId;
			currentNodeId = (long)matchResponse.nodeId;
			manager.OnMatchCreate(matchResponse);
		}
		else
		{
			//		text.text = "Create match failed";
		}
	}
	
	public void OnMatchList(ListMatchResponse matchListResponse)
	{
		if (matchListResponse.success && matchListResponse.matches != null && matchListResponse.matches.Count > 0) {
			foreach (var match in matchListResponse.matches) {
				if (match.currentSize > 0) {
					networkMatch.JoinMatch (matchListResponse.matches [0].networkId, "", OnMatchJoined);
					return;
				}
			}
		}
		CreateMatch();
	}
	
	public void OnMatchJoined(JoinMatchResponse matchJoin)
	{
		if (matchJoin.success)
		{
			matchCreated = false;
			currentNetId = (long)matchJoin.networkId;
			currentNodeId = (long)matchJoin.nodeId;
			manager.OnMatchJoined(matchJoin);
			//            Utility.SetAccessTokenForNetwork(matchJoin.networkId, new NetworkAccessToken(matchJoin.accessTokenString));
			//            NetworkClient myClient = new NetworkClient();
			//            myClient.RegisterHandler(MsgType.Connect, OnConnected);
			//            myClient.Connect(new MatchInfo(matchJoin));
		}
		else
		{
			//			text.text = "Join match failed";
		}
	}
	
	public void OnConnected(NetworkMessage msg)
	{
		//		text.text = "Connected!";
		
	}
	
	void OnApplicationQuit() {
		if (matchCreated) {
			manager.matchMaker.DestroyMatch ((NetworkID)currentNetId, OnDestroyMatch);
		} else {
			DropConnectionRequest dropReq = new DropConnectionRequest();
			dropReq.networkId = (NetworkID)currentNetId;
			dropReq.nodeId = (NodeID)currentNodeId;
			manager.matchMaker.DropConnection(dropReq, OnConnectionDrop);
		}
	}
	
	void OnDestroyMatch(BasicResponse resp) {
		
	}
	
	void OnConnectionDrop(BasicResponse resp) {
		
	}
	
}