﻿using UnityEngine;
using System.Collections;
using uWaveAlgorithm;
using System.Text;

public class GestureDetection : MonoBehaviour {

    private Facade gestureRecognizer;
	private Gyroscope gyro;
	private Vector3 acc;
	private bool init;
	private const float TURNING = 12f;

//    private const string templates = "Motionless,\n" +
//                "0|0|0,\n" +
//                "Fire,\n" +
//                "1|1|3,2|1|5,2|1|8,3|1|10,3|2|11,2|2|9,1|2|6,-2|1|-2,-5|-2|-9,-6|-3|-13,-5|-3|-14,-5|-2|-15,-3|-1|-12,-3|1|-7,-3|1|1,-2|2|6,-1|2|7,1|1|6,2|-1|4,2|-1|1,1|-2|-1,-1|-2|-1,-1|-2|1,-1|-1|1,1|1|1,1|1|1,1|1|1,\n" +
//                "1|-1|4,2|1|8,3|1|11,3|2|11,3|2|8,1|2|4,-2|-1|-2,-3|-2|-6,-4|-3|-9,-4|-3|-11,-4|-3|-12,-3|-2|-12,-3|1|-8,-3|1|-6,-3|2|-3,-3|2|2,-2|2|3,-1|1|3,-1|1|3,1|-1|2,1|-2|2,1|-2|2,1|-1|2,1|-1|2,1|-1|1,1|-1|1,1|-1|2,1|-1|2,1|1|2,1|1|2,\n" +
//                "1|-1|2,1|-1|5,2|1|8,3|1|11,3|1|11,2|1|7,-1|1|1,-3|-1|-5,-5|-2|-9,-6|-3|-11,-6|-2|-12,-4|-1|-11,-3|1|-8,-2|1|-4,-2|2|-1,-2|2|2,-1|1|3,-1|1|2,1|-1|2,1|-1|2,1|-1|2,1|-1|2,";


    // Use this for initialization
    void Start()
    {
		Time.fixedDeltaTime = 0.002f;
		var templates = new StringBuilder ();
		templates.AppendLine ("Motionless,");
		templates.AppendLine ("Fire,");
		templates.AppendLine ("1|1|2,1|-1|5,2|-2|12,2|-1|16,2|2|16,-1|3|13,-3|2|5,-6|-1|-5,-7|-2|-11,-7|-2|-14,-7|-3|-15,-6|-3|-15,-5|-3|-13,-4|-2|-11,-3|-1|-8,-2|1|-4,-1|1|-1,1|2|2,1|1|2,");
		templates.AppendLine ("-1|-2|-1,-2|-2|1,-2|-2|1,-1|-1|-1,-1|-1|-1,1|-1|-1,2|1|-1,1|1|-1,1|2|-2,-1|2|-2,-1|2|-2,-2|2|-2,-1|1|-2,-1|1|-1,-1|1|2,1|1|4,2|1|8,2|-1|11,2|1|11,1|1|6,1|1|2,-4|1|-11,-4|1|-12,-4|-1|-13,-3|1|-11,-2|1|-5,-2|2|1,-2|1|4,-2|1|5,-1|-1|4,1|-1|2,1|-1|1,1|-1|1,");
		templates.AppendLine ("1|-1|5,1|-2|10,2|-2|10,1|-1|6,-2|2|-3,-3|3|-9,-4|1|-11,-3|-1|-11,-2|-1|-9,-1|1|-4,-1|1|-1,-1|1|2,-1|-1|2,-1|-1|2,1|-1|1,1|-1|1,1|-1|1,");
		templates.Append("1|-1|3,1|-1|3,1|1|3,1|1|4,1|1|5,1|1|5,1|1|6,2|1|8,2|1|9,2|1|7,2|1|2,-1|2|-5,-3|1|-10,-4|1|-12,-4|-1|-12,-4|-1|-10,-3|-1|-4,-3|-1|2,-2|-1|6,-1|-1|6,1|-1|6,2|-1|4,2|1|2,2|1|1,");
        gestureRecognizer = new Facade(1);
        gestureRecognizer.ImportGestureLibrary(templates.ToString());
        gestureRecognizer.GestureRecognized += ActOnGesture;

		var results = gestureRecognizer.ExportGestureLibrary ();

		if (SystemInfo.supportsGyroscope) {
			gyro = Input.gyro;
			gyro.enabled = true;
			init = true;
		}
	
    }

    void ActOnGesture(object sender, GestureRecognizedEventArgs args)
    {
        if (!args.IsActualGesture) return;

        var id = args.GestureId.ToLowerInvariant();

        switch (id)
        {
            case "fire":
                Debug.Log("FIRING DETECTED. PEW PEW PEW!");
                break;
            default:
                break;
        }

    }
	
	// Update is called once per frame
	void Update () {
		var temp = Input.acceleration.normalized;
		temp.z = 0;
		temp = temp.normalized;
		var angle = Mathf.Sin((temp.x) / (1 * temp.y));
		angle = angle * 180 / (2 * Mathf.PI);


		if (angle > TURNING) {
			bool left = true;
		} else if (angle < -1 * TURNING) {
			bool right = true;

		} else if (Input.touches.Length > 0) {
			bool forward = true;
		} else {
			bool nothing = true;
		}
	}

    void FixedUpdate()
    {
		if (init) {
			gestureRecognizer.AccelerometerInput (gyro.userAcceleration.x, gyro.userAcceleration.y, gyro.userAcceleration.z);
		}
    }
}
